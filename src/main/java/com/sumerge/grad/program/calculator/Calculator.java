package com.sumerge.grad.program.calculator;

import com.sumerge.grad.program.operation.ArithmeticOperation;
import com.sumerge.grad.program.operation.AddOperation;
import com.sumerge.grad.program.operation.SubtractOperation;
import com.sumerge.grad.program.operation.MultiplyOperation;
import com.sumerge.grad.program.operation.DivideOperation;

public class Calculator {

    private ArithmeticOperation operation;

    public Double add(Double n1, Double n2) {
        operation = new AddOperation();
        return operation.performOperation(n1, n2);
    }

    public Double subtract(Double n1, Double n2) {
        operation = new SubtractOperation();
        return operation.performOperation(n1, n2);
    }

    public Double multiply(Double n1, Double n2) {
        operation = new MultiplyOperation();
        return operation.performOperation(n1, n2);
    }

    public Double divide(Double n1, Double n2) {
        operation = new DivideOperation();
        return operation.performOperation(n1, n2);
    }
}
