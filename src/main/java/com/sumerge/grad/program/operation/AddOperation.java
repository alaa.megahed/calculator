package com.sumerge.grad.program.operation;

public class AddOperation implements ArithmeticOperation {
    @Override
    public Double performOperation(Double n1, Double n2) {
        return n1 + n2;
    }
}
